# dump-innodb-files

Give me a .frm and a .ibd, I'll give you a .sql

# What is that for?

- You've got a MySQL 5.5, 5.6 or 5.7,
- `SHOW TABLES;` shows you a table, but a `DESC tablename;` and a `SELECT * FROM tablename;` returns a 1146 code, with a `Table 'dbname.tablename' doesn't exist` message,
- This table is an InnoDB one,
- You activated the innodb_file_per_table option.

If you fulfilled all those conditions, first, read the MySQL [5.5](https://dev.mysql.com/doc/refman/5.5/en/innodb-multiple-tablespaces.html), [5.6](https://dev.mysql.com/doc/refman/5.6/en/innodb-multiple-tablespaces.html) or [5.7](https://dev.mysql.com/doc/refman/5.7/en/innodb-multiple-tablespaces.html) documentation about InnoDB File-Per-Table tablespaces, to be sure you know what you're doing.

Then, know that using this HAVE to be a desperate attempt to get your data back. If you have a way to retrieve your system tablespace (ibdata1), do it now, as ignoring it will lead to data loss.

OK, you're still there, so I'll tell you how to use dump-innodb-files, and how it works.

# Use

In the same directory, put all this repository's files, your .frm and your .ibd files. Be sure you have a running Docker. Then, launch:
```
$ ./dump-innodb-files yourtablename 5.5 # If your files come from a MySQL 5.5 server
$ ./dump-innodb-files yourtablename 5.5 --verbose # If you want more details
$ ./dump-innodb-files yourtablename 5.6 # If your files come from a MySQL 5.5 server
$ ./dump-innodb-files yourtablename 5.6 --verbose # If you want more details
$ ./dump-innodb-files yourtablename 5.7 # If your files come from a MySQL 5.7 server
$ ./dump-innodb-files yourtablename 5.7 --verbose # If you want more details
```

If everything is right, you'll be able to perform a dump of the table in order to import it to your MySQL server.

# Typical execution with 5.5 version

```
01. $ ./dump-innodb-files testtbl2 5.5
02. -- dump-innodb-files-5.5:1.1 image exists
03. -- Running dump-innodb-files-5.5 container...
04. 96fcaddf84dba3604b7244b430dff34448c021bad189a1700c1c104830e5ae10
05. -- Done
06. -- Retrieving table structure from testtbl2.frm...
07. -- Done: see CLKmHX9UaV.sql
08. -- Importing testtbl2.ibd...
09. -- Import failed. Let's see if it is because of a space ID mismatch...
10. Tablespace ID in the testtbl2.ibd FIL header: 00000002
11. That matches what the MySQL error log says
12. According to the MySQL error log, this value should be 00000001. Changing it...
13. Done
14. Tablespace ID in the testtbl2.ibd FSP header: 00000002
15. That matches what the MySQL error log says
16. According to the MySQL error log, this value should be 00000001. Changing it...
17. Done
18. Fixing testtbl2.ibd checksums, step 1...
19. Done
20. Fixing testtbl2.ibd checksums, step 2...
21. Done
22. Importing testtbl2 tablespace...
23. Done
22. -- Done
23. -- Dumping testtbl2...
24. dump-innodb-files-5.5
25. -- Done: see 8aIYVYNWBo.sql
26. -- Mr. Clean says: do not forget to clean your dump-innodb-files-5.5 container and dump-innodb-files-5.5:1.1 image.
```

02: On the first execution of the dump-innodb-files command, you'll have much more logs. This is the step of the Docker image building. An environment with a MySQL 5.5 server, MySQL and Percona tools and the libs they require.

03: Create a Docker container running with this image.

06: Use mysqlfrm to retrieve the table structure using the .frm file. Create a .sql file with the structure. If everything goes well, we'll be able to dump the table structure and data, and you won't need this file. If something bad occurs, you'll have, at least, the structure of your table.

08: Create the table using the mysqlfrm result. Discard tablespace (to remove the empty .ibd file), copy the .ibd file, and try to import it.

09: Importing the IBD file failed. In most cases, this is because the space ID in the file (here, 2, what means this table was probably the second one created on the source server) does not match the ID in the system tablespace (here, 1). If so, logs like `InnoDB: Error: tablespace id and flags in file ... are ... and ..., but in the InnoDB data dictionary they are ... and ....` appear in the MySQL error log.

10: Change the space ID in the [FIL header](https://github.com/jeremycole/innodb_diagrams/blob/master/images/InnoDB_Structures/FIL%20Header%20and%20Trailer.png) section of the [IBD file](https://github.com/jeremycole/innodb_diagrams/blob/master/images/InnoDB_Structures/IBD%20File%20Overview.png).

14: Change the space ID in the [FSP header](https://github.com/jeremycole/innodb_diagrams/blob/master/images/InnoDB_Structures/FSP%20Header.png) section of the IBD file.

09-17: Note that this behaviour only concerns MySQL 5.5 (even on the very last version, the 5.5.62). This had been fixed from MySQL 5.6.

18: Use the Percona innochecksum tool to fix both headers sections checksums (we have to as we changed data in those headers).

20: Launch the Percona innochecksum again as it stops on the first mismatch, and as we changed the ID in 2 different sections.

23: Set `innodb_force_recovery = 6` to the MySQL configuration and restart the container. Trying to dump the table without this, or trying lower force recovery levels can lead to a crash of the MySQL instance. This crash can corrupt your datas more than they already are. While done, dump the table.

26: As Mr Clean said, do not forget to clean your container:

```
$ docker stop dump-innodb-files-5.5
$ docker rm dump-innodb-files-5.5
```

# Typical execution with 5.7 version

```
$ ./dump-innodb-files testtbl2 5.7
-- dump-innodb-files-5.7:1.1 image exists
-- Running dump-innodb-files-5.7 container...
c006e2926c3f5c1ca199987d341f2c03a6d5c94484ece8746c2468a82b2c266d
-- Done
-- Retrieving table structure from testtbl2.frm...
-- Done: see SB3dRrjCKe.sql
-- Importing testtbl2.ibd...
-- Done
-- Dumping testtbl2...
dump-innodb-files-5.7
-- Done: see d9IYNMNWBf.sql
-- Mr. Clean says: do not forget to clean your dump-innodb-files-5.7 container and dump-innodb-files-5.7:1.1 image.
```

Read the "Typical execution with 5.5 version" section to know exactly how it works. As you can see, the tablespace ID mismatch issue had been fixed (from MySQL 5.6).
