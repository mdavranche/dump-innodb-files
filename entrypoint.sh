#!/bin/bash

# This find command is a workaround to the bug encountered with MySQL 5.7.22
# with Docker and the overlay storage driver. With this configuration,
# attempting to start mysqld leads to this error:
#
# [ERROR] Fatal error: Can't open and lock privilege tables: Table storage
# engine for 'user' doesn't have this option
#
# The goal of this find command is to read the /var/lib/mysql data from the
# container's overlay directories (MergedDir, UpperDir and WorkDir) instead of
# the image's one (LowerDir).
#
# Naturally, this command can NOT be moved to the Dockerfile.
#
# The issue had been reproduced using overlay storage-driver on Docker 1.12.3
# and Docker 17.06, both running on a 4.14.19 kernel. It had NOT been
# reproduced using the same Docker and kernel versions with an overlay2
# storage-driver.
#
# The same behavior had been verified with the MySQL 5.5.60 version with the
# following error:
#
# Can't open and lock privilege tables: Got error 140 from storage engine
find /var/lib/mysql -exec touch {} \;

/usr/sbin/mysqld --user=root
