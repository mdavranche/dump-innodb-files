#!/usr/bin/python
# Change IBD file space IDs to the one specified in MySQL error log

import os
import re
import subprocess
import sys

# Parse MySQL error log to retrieve table name and tablespace IDs
try:
  fd = open("/var/log/mysql/error.log", "r")
  content = fd.readlines()
  fd.close()
except Exception as e:
  print("ERROR: " + str(e))
  sys.exit(1)
r = re.search("InnoDB: Error: tablespace id and flags in file '\./plops/(.+)\.ibd' are (\d+) and \d+, but in the InnoDB\nInnoDB: data dictionary they are (\d+) and \d+.", ''.join(content), re.MULTILINE)
if not r:
  print("ERROR: Can not parse MySQL error log")
  sys.exit(1)
tableName = r.groups()[0]
spaceIDLog1 = int(r.groups()[1])
spaceIDLog2 = int(r.groups()[2])

# Change tablespace ID in IBD file / FIL header
fd = open("/var/lib/mysql/plops/" + tableName + ".ibd", "rb+")
fd.seek(34)
spaceIDFile = fd.read(4).encode('hex_codec')
print "Tablespace ID in the " + tableName + ".ibd FIL header: " + spaceIDFile
if spaceIDFile == '%08x' % spaceIDLog1:
  print "That matches what the MySQL error log says"
else:
  print "ERROR: That does not match what the MySQL error log says"
  sys.exit(1)
print "According to the MySQL error log, this value should be " + '%08x' % spaceIDLog2 + ". Changing it..."
fd.seek(34)
if spaceIDLog2 <= 16777215:
  fd.write(bytearray([0]))
if spaceIDLog2 <= 65535:
  fd.write(bytearray([0]))
if spaceIDLog2 <= 255:
  fd.write(bytearray([0]))
fd.write(bytearray([spaceIDLog2]))
print "Done"

# Change tablespace ID in IBD file / FSP header
spaceIDFile = fd.read(4).encode('hex_codec') # Position in file is 38, the beginning of the FSP header
print "Tablespace ID in the " + tableName + ".ibd FSP header: " + spaceIDFile
if spaceIDFile == '%08x' % spaceIDLog1:
  print "That matches what the MySQL error log says"
else:
  print "ERROR: That does not match what the MySQL error log says"
  sys.exit(1)
print "According to the MySQL error log, this value should be " + '%08x' % spaceIDLog2 + ". Changing it..."
fd.seek(38)
if spaceIDLog2 <= 16777215:
  fd.write(bytearray([0]))
if spaceIDLog2 <= 65535:
  fd.write(bytearray([0]))
if spaceIDLog2 <= 255:
  fd.write(bytearray([0]))
fd.write(bytearray([spaceIDLog2]))
fd.close()
print "Done"

# Fix IBD file checksums
for step in range(1, 3):
  print "Fixing " + tableName + ".ibd checksums, step " + str(step) + "..."
  try:
    FNULL = open(os.devnull, 'w')
    subprocess.call([ "/usr/bin/innochecksum_changer", "-f", "/var/lib/mysql/plops/" + tableName + ".ibd" ], shell = False, stdout=FNULL, stderr=FNULL)
    FNULL.close()
  except Exception as e:
    print "ERROR - innochecksum command failed:", e
    sys.exit(1)
  print "Done"

# Import tablespace
print "Importing " + tableName + " tablespace..."
try:
  FNULL = open(os.devnull, 'w')
  subprocess.call([ "/usr/bin/mysql", "plops", "-e", "ALTER TABLE \`" + tableName + "\` IMPORT TABLESPACE;" ], shell = False, stdout=FNULL, stderr=FNULL)
  FNULL.close()
except Exception as e:
  print "ERROR - mysql command failed:", e
  sys.exit(1)
print "Done"
