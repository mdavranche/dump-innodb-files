#!/bin/bash

# Print usage and exit 1
f_usage()
{
  echo "Usage: ${0} table_name 5.5|5.6|5.7 [--verbose]"
  exit 1
}

# Exit 1 if last return code != 0
f_exit_on_error()
{
  if [ "${1}" -ne 0 ]
  then
    echo "-- FAILED"
    exit 1
  fi
}

# Print ${1} if --verbose option had been set
f_print()
{
  if [ "${VERBOSE}" -eq 1 ]
  then
    echo "   ${1}"
  fi
}

# Set TABLE, VERSION and VERBOSE from arguments
if [ ${#} -eq 3 ]
then
  if [ "${3}" != "--verbose" ]
  then
    f_usage
  fi
  VERBOSE=1
else
  if [ ${#} -ne 2 ]
  then
    f_usage
  fi
  VERBOSE=0
fi
TABLE=${1}
VERSION=${2}
case ${VERSION} in
  '5.5') TAG='1.10'
         ;;
  '5.6') TAG='1.5'
         ;;
  '5.7') TAG='1.8'
         ;;
  *)     f_usage
         exit 1
         ;;
esac
IMAGE="dump-innodb-files-${VERSION}:${TAG}"

# Check FRM and IBD files
if [ ! -f "${TABLE}.frm" ]
then
  echo "ERROR: ${TABLE}.frm does not exist"
  exit 1
fi
if [ ! -f "${TABLE}.ibd" ]
then
  echo "ERROR: ${TABLE}.ibd does not exist"
  exit 1
fi

# Build ${IMAGE} image if needed
TMP=$(docker images -q "${IMAGE}")
if [ -z "${TMP}" ]
then
  echo "-- ${IMAGE} image does not exist. Building..."
  MYSQL_IMAGE="tmp-mysql-${VERSION}-${TAG}"
  docker build --tag "${MYSQL_IMAGE}" -f "Dockerfile-${VERSION}" .
  f_exit_on_error ${?}
  docker build --build-arg MYSQL_IMAGE="${MYSQL_IMAGE}" --tag "${IMAGE}" -f "Dockerfile-tools" .
  f_exit_on_error ${?}
  docker rmi "${MYSQL_IMAGE}"
  f_exit_on_error ${?}
  echo "-- Done"
else
  echo "-- ${IMAGE} image exists"
fi
docker tag "${IMAGE}" "dump-innodb-files-${VERSION}:latest"

# Run dump-innodb-files-${VERSION} container
if ! docker inspect --format='{{.Name}}' "dump-innodb-files-${VERSION}" >/dev/null 2>&1
then
  echo "-- Running dump-innodb-files-${VERSION} container..."
  docker run --detach --name "dump-innodb-files-${VERSION}" "${IMAGE}"
  sleep 10
  echo "-- Done"
else
  echo "ERROR: Somebody is using dump-innodb-files-${VERSION} or dump-innodb-files-${VERSION} had not been cleaned"
  exit 1
fi

# Retrieve table structure from FRM file
STRUCTURE=$(mktemp XXXXXXXXXX.sql)
echo "-- Retrieving table structure from ${TABLE}.frm..."
f_print "Creating the \`plops\` database..."
docker exec "dump-innodb-files-${VERSION}" mysql -e "CREATE database IF NOT EXISTS plops;"
f_exit_on_error ${?}
f_print "OK"
f_print "Copying the ${TABLE}.frm file..."
docker cp "${TABLE}.frm" "dump-innodb-files-${VERSION}:/var/lib/mysql/plops/"
f_exit_on_error ${?}
f_print "OK"
f_print "Retrieving the instance password..."
PASSWORD=$(docker exec "dump-innodb-files-${VERSION}" cat /root/.password)
f_exit_on_error ${?}
f_print "OK"
f_print "Launching mysqlfrm..."
docker exec "dump-innodb-files-${VERSION}" bash -c "mysqlfrm --server=root:${PASSWORD}@localhost:/var/run/mysqld/mysqld.sock /var/lib/mysql/plops/${TABLE}.frm --port 4242 --user=root > /root/create.sql 2>/dev/null"
f_exit_on_error ${?}
f_print "OK"
f_print "Creating ${STRUCTURE}..."
docker exec "dump-innodb-files-${VERSION}" cat /root/create.sql > "${STRUCTURE}"
f_exit_on_error ${?}
f_print "OK"
echo "-- Done: see ${STRUCTURE}"

# Retrieve data from IBD file
echo "-- Importing ${TABLE}.ibd..."
f_print "Creating ${TABLE}..."
docker exec "dump-innodb-files-${VERSION}" bash -c "rm /var/lib/mysql/plops/${TABLE}.frm && grep -v '^WARNING' /root/create.sql | grep -v '^#' | sed 's/^$/;/' > /root/create.sql2 && mysql plops < /root/create.sql2"
f_exit_on_error ${?}
f_print "OK"
f_print "Discarding ${TABLE} tablespace..."
docker exec "dump-innodb-files-${VERSION}" mysql plops -e "ALTER TABLE \`${TABLE}\` DISCARD TABLESPACE;" > /dev/null 2>&1
f_exit_on_error ${?}
f_print "OK"
f_print "Copying the ${TABLE}.ibd file..."
docker cp "${TABLE}.ibd" "dump-innodb-files-${VERSION}:/var/lib/mysql/plops/"
f_exit_on_error ${?}
f_print "OK"
f_print "Importing ${TABLE} tablespace..."
if ! docker exec "dump-innodb-files-${VERSION}" mysql plops -e "ALTER TABLE \`${TABLE}\` IMPORT TABLESPACE;" > /dev/null 2>&1
then
  if [ "${VERSION}" != "5.5" ]
  then
    echo "-- Import failed"
    FAILED=1
  else
    echo "-- Import failed. Let's see if it is because of a space ID mismatch..."
    if ! docker exec "dump-innodb-files-${VERSION}" python /root/changeID.py
    then
      echo "-- Looks like you have another problem I can not handle. Check the dump-innodb-files-${VERSION}'s /var/log/mysql/error.log"
      FAILED=1
    else
      FAILED=0
    fi
  fi
  if [ ${FAILED} -eq 1 ]
  then
    echo "-- At least, you have the table structure here: ${STRUCTURE}"
    echo "-- Mr. Clean says: do not forget to clean your dump-innodb-files-${VERSION} container and ${IMAGE} image."
    exit 1
  fi
fi
echo "-- Done"

# Dump the table
echo "-- Dumping ${TABLE}..."
f_print "Adding \"innodb_force_recovery = 6\" to the my.cnf..."
docker exec "dump-innodb-files-${VERSION}" sed -i 's/\[mysqld\]/\[mysqld\]\ninnodb_force_recovery = 6/' /etc/mysql/my.cnf
f_exit_on_error ${?}
f_print "OK"
f_print "Restarting dump-innodb-files-${VERSION}..."
docker restart "dump-innodb-files-${VERSION}"
f_exit_on_error ${?}
sleep 10
f_print "OK"
f_print "Dumping ${TABLE}..."
FULLDUMP=$(mktemp XXXXXXXXXX.sql)
if ! docker exec "dump-innodb-files-${VERSION}" mysqldump plops "${TABLE}" --routines --events > "${FULLDUMP}"
then
  echo "-- Looks like you have a problem I can not handle. Check the dump-innodb-files-${VERSION}'s /var/log/mysql/error.log"
  echo "-- At least, you have the table structure here: ${STRUCTURE}, and a partial dump there: ${FULLDUMP}"
  echo "-- Mr. Clean says: do not forget to clean your dump-innodb-files-${VERSION} container and ${IMAGE} image."
  exit 1
fi
f_print "OK"
echo "-- Done: see ${FULLDUMP}"
echo "-- We don't need the ${STRUCTURE} file anymore, removing it..."
rm "${STRUCTURE}" || exit 1
echo "-- Done"
echo "-- We don't need the dump-innodb-files-${VERSION} container anymore, removing it..."
docker stop "dump-innodb-files-${VERSION}" || exit 1
docker rm "dump-innodb-files-${VERSION}" || exit 1
echo "-- Done"
echo "-- Mr. Clean says: do not forget to clean your ${IMAGE} image."
